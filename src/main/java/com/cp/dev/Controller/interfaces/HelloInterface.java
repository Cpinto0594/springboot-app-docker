package com.cp.dev.Controller.interfaces;

import org.springframework.web.bind.annotation.GetMapping;

public interface HelloInterface {

    @GetMapping("/hello")
    String hello();

}
