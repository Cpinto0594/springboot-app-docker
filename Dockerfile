FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
EXPOSE 8080
WORKDIR /usr/src/app-service
VOLUME /usr/src/app-service/app-vol
ENTRYPOINT ["java","-jar","/app.jar"]