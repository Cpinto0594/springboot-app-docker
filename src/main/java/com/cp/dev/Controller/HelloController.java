package com.cp.dev.Controller;

import com.cp.dev.Controller.interfaces.HelloInterface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController
        implements HelloInterface {


    @Override
    public String hello() {
        return "Hello";
    }
}
